int numTones = 7;
int tones[] = { 260, 294,330, 349, 370, 392, 420};
int constTones[] = { 260, 294,330, 349, 370, 392, 420};

int pin = 8;
int pp1 = 1;
int pp2 = 2;
int pp3 = 3;
int pp4 = 4;
int pp5 = 5;
int pp6 = 6;
int potentiometer = 0;
int vp1, vp2, vp3, vp4, vp5, vPot;
int ovp1,ovp2,ovp3,ovp4,ovp5;


void setup()
{
 Serial.begin(9600);
   ovp1= analogRead(pp1);
  ovp2 = analogRead(pp2);
  ovp3= analogRead(pp3);
  ovp4 = analogRead(pp4);
  ovp5= analogRead(pp5);
  ovp6 = analogRead(pp6);
  }

void loop() {
getValues();
int noteChange = map(vPot,0,1024,0,100);
for (int i = 0; i < numTones; i++)
  {
    if(noteChange >= 50){
    tones[i] = constTones[i] + noteChange;
    }
      if(noteChange <= 50){
    tones[i] = constTones[i] - noteChange;
    }
    
  }
 
play();

 

  delay(100); 
}
void play(){
   if(vp1< ovp1-450){
    tone(pin, tones[0]);
   
  }else  if(vp2< ovp2-450){
    tone(pin, tones[1]);
    
  }else  if(vp3< ovp3-450){
    tone(pin, tones[2]);
   
  }else  if(vp4< ovp4-450){
    tone(pin, tones[3]);
   
  }else  if(vp5< ovp5-450){
    tone(pin, tones[4]);

  } 
  else{
    noTone(pin);
  }

}

void getValues(){
   vp1= analogRead(pp1);
  vp2 = analogRead(pp2);
  vp3= analogRead(pp3);
  vp4 = analogRead(pp4);
  vp5= analogRead(pp5);
  vp6 = analogRead(pp6);
  vPot = analogRead(potentiometer);
}
